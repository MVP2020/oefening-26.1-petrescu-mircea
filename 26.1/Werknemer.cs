﻿using System;

namespace _26._1
{
    class Werknemer
    {
        private double _loon;
        private string _naam;
        private string _voornaam;

        public Werknemer(string naam, string voornaam, double loon)
        {
            Naam = naam;
            Voonaam = voornaam;
            Loon = loon;
        }

        public double Loon { get; set; }
        public string Naam { get; set; }

        public string Voonaam { get; set; }

        public virtual double Verdiensten()
        {
            return Loon;
        }

        //public override bool Equals(obejct obj)
        // {

        // }
        public override string ToString()
        {
            return Naam + "        " + Voonaam + "        " + Verdiensten() + " €" + Environment.NewLine;
        }
    }
}
