﻿using System;

namespace _26._1
{
    class UurWerker : Werknemer
    {
        private double _uren;

        public double Uren { get; set; }

        public UurWerker(string naam, string voornaam, double loon, int aantalUren) : base(naam, voornaam, loon)
        {
            Uren = aantalUren;
        }


        public override double Verdiensten()
        {
            return Loon * Uren;
        }

        public override string ToString()
        {
            return Naam + "        " + Voonaam + "        " + Verdiensten() + " €" + "        UurWerker" + Environment.NewLine;
        }

    }
}
