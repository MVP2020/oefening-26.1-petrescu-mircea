﻿using System;

namespace _26._1
{
    class StukWerker : Werknemer
    {
        private int _aantal;

        public int Aantal { get; set; }
        public StukWerker(string naam, string voornaam, double loonPerStuk, int aantal) : base(naam, voornaam, loonPerStuk)
        {
            Aantal = aantal;

        }




        public override double Verdiensten()
        {
            return Loon * Aantal;
        }

        public override string ToString()
        {
            return Naam + "        " + Voonaam + "        " + Verdiensten() + " €" + "         StukWerker" + Environment.NewLine;
        }
    }


}
