﻿using System;
using System.Windows;

namespace _26._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void btnToevoegen_Click(object sender, EventArgs e)
        {
            if (rdCommissie.IsChecked == true)
            {

                CommissieWerker commisieWerker = new CommissieWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), Convert.ToDouble(txtLoon.Text), Convert.ToDouble(txtCommissie.Text), Convert.ToInt32(txtAantal.Text));
                txtResult.Text += commisieWerker.ToString();



            }
            else if (rdStuk.IsChecked == true)
            {
                StukWerker stukWerker = new StukWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loonPerStuk: (Convert.ToDouble(txtLoon.Text)), aantal: Convert.ToInt32(txtAantal.Text));
                txtResult.Text += stukWerker.ToString();
            }
            else if (rdUur.IsChecked == true)
            {
                UurWerker uurWerker = new UurWerker(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loon: (Convert.ToDouble(txtLoon.Text)), aantalUren: (Convert.ToInt32(txtAantal.Text)));
                txtResult.Text += uurWerker.ToString();
            }
            else if (rdWerknemer.IsChecked == true)
            {
                Werknemer werknemer = new Werknemer(naam: (txtNaam.Text), voornaam: (txtVoornaam.Text), loon: (Convert.ToDouble(txtLoon.Text)));
                txtResult.Text += werknemer.ToString();
            }
            else
            {
                MessageBox.Show("Please select an employee!");
            }
        }

    }
}
