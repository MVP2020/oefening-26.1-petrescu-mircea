﻿using System;

namespace _26._1
{
    class CommissieWerker : Werknemer
    {


        private int _aantal;
        private double _commissie;

        public int Aantal { get; set; }

        public double Commissie { get; set; }

        public CommissieWerker(string naam, string voornaam, double loon, double commissie, int aantal) : base(naam, voornaam, loon)
        {
            Naam = naam;
            Voonaam = voornaam;
            Loon = loon;
            Commissie = commissie;
            Aantal = aantal;
        }


        public override double Verdiensten()
        {
            return Loon + Aantal * Commissie;
        }
        public override string ToString()
        {
            return Naam + "        " + Voonaam + "        " + Verdiensten() + " €" + "        CommissieWerker" + Environment.NewLine;
        }



    }
}
